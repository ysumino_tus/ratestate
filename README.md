# README #

This repository is to create the figure shown in the https://arxiv.org/abs/2012.01799.
To compile these source codes written in C++17, make command will generate executable codes "rs" if intel c++ compiler is available. Instead, any other C++ compiler (like GCC) compatiable with C++11 will generate executable file. The executable file requires 3 or 4 arguments, (G:5 or 0.5) (beta:-1, -0.5, 0.5-6, or -0.1, -0.05-0.6) (vl:4, 3, 2, 1, 0.5, 0.25) or (G:5 or 0.5) (beta:-1, -0.5, 0.5-6, or -0.1, -0.05-0.6) (vl:4, 3, 2, 1, 0.5, 0.25) (file) where (file) can be loaded to continue simulation using "*_fin.dat" file created when the simulation finished. The details of the simulation condition is descrived in the paper.
The typical calculation time is 600 s and 7000 s with Intel(R) Core(TM) i7-8700K CPU for the one dataset for the fig3 and fig5, respectively.
