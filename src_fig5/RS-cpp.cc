#include<fstream>
#include<iostream>
#include<sstream>
#include<math.h>
#include<cstdlib>
#include <time.h>
#include <iomanip>
#include <string.h>
//#include"dSFMT.h"
//#include"gasdev.h"
#include <random>
#define MAX_LEN 100


main(int argc,char **argv){
    //related with file name preparation
    int len;
    char *words[128], *cp,*foot,*cp2;
    char strtmp[512], strtmp2[512];
    const char *delim ="_";
    const char *delim2 =".";
    //till here

    
    if((argc!=4)&&(argc!=5)){
        std::cout<<"**************************************************************************************************\n";
        std::cout<<"*****************error  !!************************************************************************\n";
        std::cout<<"** this program require 3 or 4 arguments                                                          \n";
        std::cout<<"**(this) (G:5 or 0.5) (beta:-1, -0.5, 0.5-6, or -0.1, -0.05-0.6) (vl:4, 3, 2, 1, 0.5, 0.25)       \n";
        std::cout<<"**or                                                                                              \n";
        std::cout<<"**(this) (G:5 or 0.5) (beta:-1, -0.5, 0.5-6, or -0.1, -0.05-0.6) (vl:4, 3, 2, 1, 0.5, 0.25) (file)\n";
        std::cout<<"**************************************************************************************************\n";
        return(1);
    }

    if(argc==5){
        strcpy(strtmp,argv[4]);
        cp=strtmp;
        for(len=0;len<MAX_LEN;len++){
            if((words[len] = strtok(cp,delim))==NULL)
                break;
            cp = NULL;
        }
        strcpy(strtmp2,words[len-1]);
        cp2=strtmp2;
        foot = strtok(cp2,delim2);
    }

    //related with random number for the initial condition
    std::random_device rnd;
	std::mt19937 mt(rnd());
    std::normal_distribution<> normran(0.,1.);
    std::uniform_real_distribution<> uniran(
		0,
		std::nextafter(1.0, std::numeric_limits<double>::max())
	);
    //till here

    int lmax=8192;
    int wmax=1;
    double dt, dx, al, gamma,t,  G, vl,beta, vs,Y,eta,taus, B,pert;
    int    skipi,n, i, j, im, jm, ip,jp, m, o, intp;

    //Time difinition
    int nmax=124098500;
    int ths=  42138000;
    int sframes =500;
    int sskip = 4;

    int tmax=(nmax-ths)/sframes-1;

    double *v, *x,*th, *vn, *xn, *thn, *vout, *out;

    clock_t start,end;
    double stime;
    time_t timer;

    dt = 0.0002;
    dx = 0.05;
    al = dt/(dx*dx);
    Y = 0.1;
    eta = 0.01;
    taus=1.;
    vs=0.1;

    //simulation parameter
    G=atof(argv[1]);
    beta=atof(argv[2]);
    vl=atof(argv[3]);
    B=1.+beta;

    v = new double[lmax];
    x = new double[lmax];
    th = new double[lmax];

    vn = new double[lmax];
    xn = new double[lmax];
    thn = new double[lmax];
    vout = new double[lmax*tmax/sskip];

    out = new double[lmax*3];

    //related with output file preparation
    std::ostringstream fh,hh;
    std::ostringstream comm;
    std::ofstream os, os2,os3,gn_com;
    std::ifstream fi;

    system("mkdir dat");
    fh.str("");
    fh<<std::setprecision(3)<<G<<"_"<<beta<<"_"<<vl<<"_"
        <<lmax/sskip<<"_"<<(tmax);
    hh.str("");
    hh<<"dat";
    pert=0.1;
    for(j=0;j < lmax; j++){
        x[j] = -(taus-beta*log(vl/vs))/G+pert*(uniran(mt)-0.5);
        v[j] = pert*(uniran(mt)-0.5);
        th[j] = -B*log(vl/vs)+pert*(uniran(mt)-0.5);
    }
    if(argc==5){

        fh.str("");
        std::cout<<words[0]<<"_"<<words[1]<<"_"<<words[2]<<"_"<<words[3]<<"_"
            <<words[4]<<"_short";
        fh<<words[0]<<"_"<<words[1]<<"_"<<words[2]<<"_"<<words[3]<<"_"
            <<words[4]<<"_short_"<<G<<"_"<<beta<<"_"<<vl<<"_"<<lmax/sskip<<"_"<<(tmax);//tau;
        std::cout<<fh.str();
        fi.open(argv[4],std::ios::in|std::ios::binary);
        i=0;
        double * readout;
        readout = new double[lmax*3];
        double tempdat;
        while(!fi.eof()){
            fi.read((char*) &tempdat, sizeof(double));
            readout[i]=tempdat;
            i++;
        }
        fi.close();
        for(i=0; i<lmax; i++){
            x[i]=readout[i];
        }
        for(i=0; i<lmax;i++){
            v[i]=readout[i+lmax];
        }
        for(i=0; i<lmax;i++){
            th[i]=readout[i+lmax*2];
        }
        delete [] readout;
    }

    start=clock();
    for (n=0; n < nmax; n++){
        t = dt * (double)n;
        /* culculation starts*/
        for(j=0; j<lmax; j++){
            jm = j-1;
            jp = j+1;
            if( jm<0 ){
                jm = lmax-1;
            }
            if( jp>lmax-1 ){
                jp = 0;
            }
            xn[j] = x[j] + dt*v[j];
            vn[j] = v[j] +dt*(-G*x[j]-(taus+th[j]+log((v[j]+vl)/vs))) +al*Y*(x[jm]+x[jp]-2.*x[j])+al*eta*(v[jm]+v[jp]-2.*v[j]);
            thn[j] = th[j]- dt*(v[j]+vl)*(th[j]+B*log((v[j]+vl)/vs));
        }	
        for(j=0; j<lmax; j++){
            x[j] = xn[j];
            v[j] = vn[j];
            th[j]=thn[j];
        }

        if(n>ths){
            if(n%sframes==0){
                for( j=0;j<lmax/sskip;j++){
                	vout[j+lmax/sskip*((n-ths)/sframes-1)]=v[j*sskip];
                }
            }
        }
    }
    for(j=0; j<lmax; j++){
        out[j] = x[j];
    }
    for(j=0; j<lmax; j++){
        out[j+lmax] = v[j];
    }
    for(j=0; j<lmax; j++){
        out[j+lmax*2] = th[j];
    }

    comm.str("");
    comm<<"./"<<hh.str()<<"/"<<fh.str()<<"_v.raw";
    os.open((comm.str()).c_str(),std::ios::binary);
    os.write((char*)vout,sizeof(double)*lmax/sskip*tmax);
    os.close();

    comm.str("");
    comm<<"./"<<hh.str()<<"/"<<fh.str()<<"_fin.dat";
    os.open((comm.str()).c_str(),std::ios::binary);
    os.write((char*)out,sizeof(double)*lmax*3);
    os.close();

    end=clock();
    stime=(double)(end-start)/(double)CLOCKS_PER_SEC;
    time(&timer);
    comm.str("");
    comm<<"log.txt";
    os.open((comm.str()).c_str(),std::ios::out | std::ios::app);
    os<<fh.str()<<"\t"<<"time:\t"<<stime<<"\tLocaltime:\t"<<ctime(&timer);
    os.close();

    delete[] v, vn,x, xn,vout,th, thn, out;

}
